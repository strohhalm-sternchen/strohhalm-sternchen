﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [SerializeField, Tooltip("The games main scene.")]
    private string main;


    // --- | Methods | -----------------------------------------------------------------------------------------------------

    /// <summary>
    /// Loads the main scene / starts the game.
    /// </summary>
    public void LoadMainScene()
    {
        SceneManager.LoadScene(main);
    }

    /// <summary>
    /// Exit the game.
    /// </summary>
    public void ExitGame()
    {
        Application.Quit();
#if (UNITY_EDITOR)
        UnityEditor.EditorApplication.isPlaying = false;
#endif
    }
}
