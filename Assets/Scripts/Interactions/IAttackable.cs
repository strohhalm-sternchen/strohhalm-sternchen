﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAttackable
{
    /// <summary>
    /// True if the object can be hit.
    /// </summary>
    bool IsHittable { get; }

    /// <summary>
    /// Deals damage to the object.
    /// </summary>
    /// <param name="damage">The damage to deal.</param>
    void Hit(int damage);
}
