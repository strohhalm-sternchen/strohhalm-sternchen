﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDrainable
{
    /// <summary>
    /// The objects transform.
    /// </summary>
    Transform transform { get; }

    /// <summary>
    /// True if the object can be drained.
    /// </summary>
    bool IsDainable { get; }

    /// <summary>
    /// The amount of blood in the object.
    /// </summary>
    float Blood { get; }

    /// <summary>
    /// Drains the object.
    /// </summary>
    /// <param name="amount">The amount of blood to drain.</param>
    /// <returns>The amount of drained blood.</returns>
    float Drain(float amount);
}
