﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IStunable
{
    /// <summary>
    /// Stuns the object.
    /// </summary>
    /// <param name="duration">The duration of the stun.</param>
    void Stun(float duration);
}
