﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBloodInteractable
{
    /// <summary>
    /// Triggers the objects reaction to blood.
    /// </summary>
    void InteractWithBlood();
}
