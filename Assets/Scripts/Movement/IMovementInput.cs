﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMovementInput
{
    /// <summary>
    /// Gets the direction the character is moving.
    /// </summary>
    /// <returns>The direction the character is moving.</returns>
    Vector3 GetDirection(); 

    /// <summary>
    /// Gets the directin the character is facing.
    /// </summary>
    /// <returns>The rotation of the character, in degrees.</returns>
    float GetRotation();

    /// <summary>
    /// Enables the controls.
    /// </summary>
    void EnableControls();

    /// <summary>
    /// Disables the controls.
    /// </summary>
    void DisableControls();
}
