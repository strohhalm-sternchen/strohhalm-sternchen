﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VampireMovement : CharacterMovement
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [Header("References")]
    [SerializeField, Tooltip("The mesh that should be rotated.")]
    protected Transform rotatingMesh;


    // --- | Variables & Properties | -------------------------------------------------------------------------------------- 

    /// <summary>
    /// The rotation the vampire tilds when moving, in degrees.
    /// </summary>
    private const float VAMPIRE_ROTATION = 20f;


    // --- | Methods | -----------------------------------------------------------------------------------------------------
    // MonoBehaviour ------------------------------------------------------------------------------

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        RotateVampire(move.Grounded().normalized);
    }

    // Movement -----------------------------------------------------------------------------------

    /// <summary>
    /// Tilds the vampire into the direction he moves.
    /// </summary>
    /// <param name="moveDirection">The direction the vampire moves.</param>
    protected void RotateVampire(Vector3 moveDirection)
    {
        Vector3 meshRotation = rotatingMesh.eulerAngles;
        rotatingMesh.localEulerAngles = Vector3.zero;
        rotatingMesh.Rotate(Vector3.right * moveDirection.z * VAMPIRE_ROTATION, Space.World);
        rotatingMesh.Rotate(Vector3.forward * -moveDirection.x * VAMPIRE_ROTATION, Space.World);
    }
}
