﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class PlayerInput : MonoBehaviour, IMovementInput
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [Header("General")] // ------------------------------------------------------------------------
    [SerializeField, Tooltip("The input type.")]
    private InputType inputType = InputType.Keyboard;
    [SerializeField, Tooltip("Vampire to controll.")]
    private Vampire vampire;

    [Header("Keyboard Inputs")]
    [SerializeField, Tooltip("The button pressed on the keyboard / mouse to attack.")]
    private KeyCode keyboardAttack = KeyCode.Mouse0;
    [SerializeField, Tooltip("The button pressed on the keyboard / mouse to drain.")]
    private KeyCode keyboardDrain = KeyCode.Mouse1;

    [Header("Gamepad Inputs")]
    [SerializeField, Tooltip("The button pressed on the gamepad / mouse to attack.")]
    private KeyCode gamepadAttack = KeyCode.JoystickButton5;
    [SerializeField, Tooltip("The button pressed on the gamepad / mouse to drain.")]
    private KeyCode gamepadDrain = KeyCode.JoystickButton4;


    // --- | Variables & Properties | -------------------------------------------------------------

    Vector2 rotation = Vector2.zero;
    private bool controlsEnabled = true;
    private Vector2 lateMousePos;
    private const float GAMEPAD_SENTSITIVITY = 0.2f;


    // --- | Methods | -----------------------------------------------------------------------------------------------------

    private void Update()
    {
        if (controlsEnabled)
        {
            if ((inputType == InputType.Keyboard && Input.GetKeyDown(keyboardAttack)) || 
                (inputType == InputType.Gamepad && Input.GetKeyDown(gamepadAttack)))
            {
                vampire.Attack();
            }
            if ((inputType == InputType.Keyboard && Input.GetKeyDown(keyboardDrain)) ||
                (inputType == InputType.Gamepad && Input.GetKeyDown(gamepadDrain)))
            {
                vampire.DrainAttack();
            }
        }
    }


    // Movment ------------------------------------------------------------------------------------

    /// <summary>
    /// Gets the direction the character should move.
    /// </summary>
    /// <returns>The direction the character should move.</returns>
    public float GetRotation()
    {
        if (inputType == InputType.Keyboard)
        {
            Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(camRay, out hit))
            {
                Vector3 delta = hit.point - transform.position;
                rotation.x = delta.x;
                rotation.y = delta.z;
                rotation = rotation.normalized;
            }
        }
        else
        {
            rotation = new Vector2(Input.GetAxisRaw("HorizontalOrientation"), Input.GetAxisRaw("VerticalOrientation"));
            if (Mathf.Abs(rotation.magnitude) < GAMEPAD_SENTSITIVITY)
            {
                Vector3 delta = transform.eulerAngles;
                rotation.x = delta.x;
                rotation.y = delta.z;
                rotation = rotation.normalized;
            }
        }
        return Mathf.Atan2(rotation.x, rotation.y) * Mathf.Rad2Deg;
    }

    /// <summary>
    /// The direction the character should face.
    /// </summary>
    /// <returns>The characters rotation in degrees.</returns>
    public Vector3 GetDirection()
    {
        Vector3 movement = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        movement.z = movement.y;
        movement.y = 0f;
        return movement;
    }

    // Enable & Disable ---------------------------------------------------------------------------

    /// <summary>
    /// Enables the controlls.
    /// </summary>
    public void EnableControls()
    {
        controlsEnabled = true;
    }

    /// <summary>
    /// Disable the controls.
    /// </summary>
    public void DisableControls()
    {
        controlsEnabled = false;
    }

    // Input --------------------------------------------------------------------------------------

    /// <summary>
    /// Sets the input type.
    /// </summary>
    /// <param name="type"></param>
    public void SetInputDevice(InputType type)
    {
        inputType = type;
    }
}
