﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles a characters movement.
/// </summary>
public class CharacterMovement : MonoBehaviour
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [Header("General")] // ------------------------------------------------------------------------
    [SerializeField, Tooltip("The speed with that the character moves.")]
    private float movespeed = 4f;
    [SerializeField, Tooltip("The decelleration when pushed back, units/sec.")]
    private float decelleration = 5f;

    [Header("References")] // ---------------------------------------------------------------------
    [SerializeField, Tooltip("The script handling the animations.")]
    private AnimationHandle animationHandle;


    // --- | Components | --------------------------------------------------------------------------------------------------

    private CharacterController controller;
    private IMovementInput input;


    // --- | Variables & Properties | --------------------------------------------------------------------------------------

    protected bool enabledMovement = true;
    /// <summary>
    /// True if the movement is enabled.
    /// </summary>
    public bool EnabledMovement
    {
        get => enabledMovement;
        set => enabledMovement = value;
    }
    /// <summary>
    /// True if the controller can move.
    /// </summary>
    private bool CanMove => EnabledMovement && force.magnitude <= 0f;

    // Velocity -----------------------------------------------------------------------------------
    protected Vector3 force;
    protected Vector3 move;
    protected float gravitiy = 0f;


    // --- | Methods | -----------------------------------------------------------------------------------------------------
    // MonoBehaviour ------------------------------------------------------------------------------

    protected virtual void Awake()
    {
        controller = GetComponent<CharacterController>();
        input = GetComponent<IMovementInput>();
    }

    protected virtual void FixedUpdate()
    {
        MoveDirection(input.GetDirection(), input.GetRotation());
    }

    // Movement -----------------------------------------------------------------------------------

    /// <summary>
    /// Move a character in a direction.
    /// </summary>
    /// <param name="moveDirection">The direction to move the character.</param>
    /// <param name="lookRotation">The rotation of the character.</param>
    public void MoveDirection(Vector3 moveDirection, float lookRotation)
    {
        bool isGrounded = controller.isGrounded || Physics.Raycast(transform.position, Physics.gravity, 0.25f);
        if (!CanMove || !isGrounded)
        {
            moveDirection = force;
            animationHandle.UpdateMove(Vector3.zero);
        }
        else if (moveDirection.magnitude > 1f)
        {
            moveDirection = moveDirection.normalized;
            animationHandle.UpdateMove(moveDirection);
        }
        // Reset gravity on floor.
        if (isGrounded && gravitiy <= 0f)
        {
            gravitiy = 0f;
            RaycastHit hit;
            if (Physics.Raycast(transform.position, Vector3.down, out hit))
            {
                moveDirection += Vector3.down * 5f;
            }
        }
        else
        {
            gravitiy += Physics.gravity.magnitude * Time.fixedDeltaTime;
        }
        // Update Rotation.
        if (CanMove)
        {
            transform.eulerAngles = transform.up * lookRotation;
        }
        move = moveDirection * movespeed;
        controller.Move((move + Physics.gravity.normalized * gravitiy) * Time.fixedDeltaTime);
        // Update force.
        if (force.magnitude > 0f)
        {
            float forceMag = force.magnitude;
            forceMag -= decelleration * Time.fixedDeltaTime;
            if (forceMag <= 0f)
            {
                forceMag = 0f;
            }
            force = force.normalized * forceMag;
        }
    }

    /// <summary>
    /// Applies force to the controller.
    /// </summary>
    /// <param name="force">The force to apply.</param>
    public void ApplyForce(Vector3 force)
    {
        this.force.x += force.x;
        this.force.z += force.z;
        gravitiy -= force.y;
    }
}
