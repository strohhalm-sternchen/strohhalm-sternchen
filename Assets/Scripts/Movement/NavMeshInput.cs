﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshInput : MonoBehaviour, IMovementInput
{
    // --- | Variables & Properties | --------------------------------------------------------------------------------------

    private Transform target;
    /// <summary>
    /// The target of the character.
    /// </summary>
    public Transform Target => target;
    private NavMeshPath path;
    private Vector3 targetPos;
    private Vector3 direction;
    private float rotation;
    private bool isChasing = false;
    private bool enabledControls = true;


    // --- | Methods | -----------------------------------------------------------------------------------------------------
    // MonoBehaviour ------------------------------------------------------------------------------

    protected void Awake()
    {
        path = new NavMeshPath();
    }

    private void Start()
    {
        targetPos = transform.position;
    }

    // Movement -----------------------------------------------------------------------------------

    /// <summary>
    /// Get the direction the character should move.
    /// </summary>
    /// <returns>The direction the character should move.</returns>
    public Vector3 GetDirection()
    {
        if (enabledControls)
        {
            if (isChasing && target)
            {
                targetPos = target.position;
            }
            NavMesh.CalculatePath(transform.position, targetPos, NavMesh.AllAreas, path);
            if (path.corners.Length > 1)
            {
                direction = path.corners[1] - transform.position;
                rotation = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
            }
            else
            {
                direction = Vector3.zero;
                rotation = transform.eulerAngles.y;
            }
        }
        else
        {
            direction = Vector3.zero;
        }
        return direction;
    }

    /// <summary>
    /// The direction the character is facing.
    /// </summary>
    /// <returns>the rotation of the character in degrees.</returns>
    public float GetRotation()
    {
        return rotation;
    }

    // Target -------------------------------------------------------------------------------------

    /// <summary>
    /// Set the target of the character.
    /// </summary>
    /// <param name="target">The characters new target.</param>
    public void SetTarget(Transform target)
    {
        if (target != null)
        {
            this.target = target;
        }
        isChasing = true;
    }

    /// <summary>
    /// Stops the movement of the character.
    /// </summary>
    public void Stop()
    {
        //target = null;
        isChasing = false;
        targetPos = transform.position;
    }

    // Enable & Disable ---------------------------------------------------------------------------

    /// <summary>
    /// Enables the controls.
    /// </summary>
    public void EnableControls()
    {
        enabledControls = true;
    }

    /// <summary>
    /// Disables the controls.
    /// </summary>
    public void DisableControls()
    {
        enabledControls = false;
    }
}
