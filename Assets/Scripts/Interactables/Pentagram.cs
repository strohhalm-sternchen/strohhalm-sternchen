﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pentagram : MonoBehaviour, IBloodInteractable
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [Header("General")]
    [SerializeField, Tooltip("The time it takes the demon to spawn.")]
    private float spawnTime = 5f;

    [Header("References")]
    [SerializeField, Tooltip("The demon prefab to spawn on the pentagram.")]
    private GameObject demonPrefab;
    [SerializeField, Tooltip("The target the demon should attack, usualy the player.")]
    private Transform demonTarget;
    [SerializeField, Tooltip("The fire particle system.")]
    private ParticleSystem fire;


    // --- | Variables & Properties | --------------------------------------------------------------------------------------

    private bool isActive = false;
    private Demon demon;


    // --- | Methods | -----------------------------------------------------------------------------------------------------
    // MonoBehaviour ------------------------------------------------------------------------------
    private void Update()
    {
        if (demon)
        {
            demon.transform.position += Vector3.up * Time.deltaTime * spawnTime;
            if (demon.transform.position.y >= transform.position.y)
            {
                demon.transform.position = transform.position;
                demon.GetComponent<CharacterMovement>().enabled = true;
                demon.SetTarget(demonTarget);
                demon = null;
            }
        }
    }

    // Interaction --------------------------------------------------------------------------------

    /// <summary>
    /// Interacts with blood.
    /// </summary>
    public void InteractWithBlood() => Activate();

    /// <summary>
    /// Activates the pentagram.
    /// </summary>
    public void Activate()
    {
        if (!isActive)
        {
            isActive = true;
            SpawnDemon();
            fire.Play();
        }
    }

    /// <summary>
    /// Spawns the demon.
    /// </summary>
    private void SpawnDemon()
    {
        demon = Instantiate(demonPrefab, transform.position + Vector3.down * 3f, Quaternion.identity).GetComponent<Demon>();
        demon.GetComponent<CharacterMovement>().enabled = false;
    }
}
