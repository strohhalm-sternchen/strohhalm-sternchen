﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spikes : MonoBehaviour, IBloodInteractable
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [Header("General")]
    [SerializeField, Tooltip("The damage the spikes deal.")]
    private int damage = 10;
    [SerializeField, Tooltip("The time until the spikes can go back up.")]
    private Cooldown cooldown = new Cooldown(2f);


    // --- | Components | --------------------------------------------------------------------------------------------------
    
    private Animator animator;
    

    // --- | Variables & Properties | --------------------------------------------------------------------------------------

    private List<IAttackable> insideObjects = new List<IAttackable>();


    // --- | Methods | -----------------------------------------------------------------------------------------------------
    // MonoBehaviour ------------------------------------------------------------------------------
    private void Awake()
    {
        animator = GetComponentInChildren<Animator>();
    }

    private void Update()
    {
        if (cooldown.IsOnCooldown)
        {
            cooldown.Update(Time.deltaTime);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        IAttackable attackable = other.GetComponent<IAttackable>();
        if (attackable != null)
        {
            insideObjects.Add(attackable);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        IAttackable attackable = other.GetComponent<IAttackable>();
        if (attackable != null)
        {
            insideObjects.Remove(attackable);
        }
    }

    // Interactions -------------------------------------------------------------------------------

    /// <summary>
    /// Interact with blood.
    /// </summary>
    public void InteractWithBlood() => SpikesUp();

    /// <summary>
    /// Move the spikes up and deal damage to objects on top.
    /// </summary>
    public void SpikesUp()
    {
        if (!cooldown.IsOnCooldown)
        {
            cooldown.Set();
            animator.SetTrigger("Up");
            CheckForDamage();
        }
    }

    /// <summary>
    /// Check for objects to deal damage to.
    /// </summary>
    private void CheckForDamage()
    {
        foreach (IAttackable attackable in insideObjects)
        {
            if (attackable == null)
            {
                insideObjects.Remove(attackable);
            }
            attackable.Hit(damage);
        }
    }
}
