﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class FieldOfView
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [SerializeField, Tooltip("The view areas range.")]
    private float radius = 1f;
    [SerializeField, Range(0f, 360f), Tooltip("The angle infront where objects will be detected.")]
    private float angle = 120f;
    [SerializeField, Tooltip("Check if there is anything inbetween the target and the center of the view.")]
    private bool checkDirectSight = true;


    // --- | Constructor | -------------------------------------------------------------------------------------------------

    /// <summary>
    /// Creates a new <see cref="FieldOfView"/> object.
    /// </summary>
    /// <param name="radius">The view areas range.</param>
    /// <param name="angle">The angle infront where objects will be detected.</param>
    /// <param name="checkDirectSight">True if the field of view should check if there is some object blocking a direct sight.</param>
    public FieldOfView(float radius, float angle, bool checkDirectSight)
    {
        this.radius = radius;
        this.angle = angle;
        this.checkDirectSight = checkDirectSight;
    }


    // --- | Methods | -----------------------------------------------------------------------------------------------------
    // Target Detection ---------------------------------------------------------------------------

    /// <summary>
    /// Gets all <see cref="Transform"/> objects with a collider in the field of view.
    /// </summary>
    /// <param name="transform">The transform of the object to cast it from.</param>
    /// <returns>The transforms inside the field of view.</returns>
    public Transform[] GetTargets(Transform transform)
    {
        return GetTargets(null);
    }
    /// <summary>
    /// Gets all <see cref="Transform"/> objects with a collider in the field of view, fitting the filter.
    /// </summary>
    /// <param name="transform">The transform of the object to cast it from.</param>
    /// <param name="filter">The fiter to compare the targets to.</param>
    /// <returns>The objects inside the field of view.</returns>
    public Transform[] GetTargets(Transform transform, Filter filter)
    {
        return GetTargets(transform.position, transform.forward, filter);
    }
    /// <summary>
    /// Gets all <see cref="Transform"/> objects with a collider in the field of view.
    /// </summary>
    /// <param name="position">The position from where to cast the view.</param>
    /// <param name="direction">The direction of the view.</param>
    /// <returns>The objects inside the field of view.</returns>
    public Transform[] GetTargets(Vector3 position, Vector3 direction)
    {
        return GetTargets(position, direction, null);
    }
    /// <summary>
    /// Gets all <see cref="Transform"/> objects with a collider in the field of view, fitting the filter.
    /// </summary>
    /// <param name="position">The position from where to cast the view.</param>
    /// <param name="direction">The direction of the view.</param>
    /// <param name="filter">The fiter to compare the targets to.</param>
    /// <returns>The objects inside the field of view.</returns>
    public Transform[] GetTargets(Vector3 position, Vector3 direction, Filter filter)
    {
        List<Transform> targets = new List<Transform>();
        foreach (Collider collider in Physics.OverlapSphere(position, radius))
        {
            if ((filter == null || filter.Compare(collider.transform)) && Vector3.Angle(direction, collider.transform.position - position) <= angle / 2f)
            {
                if (!checkDirectSight || !Physics.Linecast(position, collider.transform.position))
                {
                    targets.Add(collider.transform);
                }
            }
        }
        return targets.ToArray();
    }


    // Gizmos --------------------------------------------------------------------------------------------------------------

    /// <summary>
    /// Draws a the vield of view in the gizmos.
    /// </summary>
    /// <param name="transform">The objects transfomr from where to cast the field of view from.</param>
    /// <param name="color">The color to draw the field of view in.</param>
    public void DrawGizmos(Transform transform, Color color)
    {
        DrawGizmos(transform.position, transform.forward, color);
    }
    /// <summary>
    /// Draws a the vield of view in the gizmos.
    /// </summary>
    /// <param name="position">The position from where to cast the field of view from.</param>
    /// <param name="direction">The direction of the field of view.</param>
    /// <param name="color">The color to draw the field of view in.</param>
    public void DrawGizmos(Vector3 position, Vector3 direction, Color color)
    {
#if (UNITY_EDITOR)
        Gizmos.color = color;
        Gizmos.DrawWireSphere(position, radius);
        if (angle < 360f)
        {
            Gizmos.DrawRay(position, Quaternion.AngleAxis(angle / 2f, Vector3.up) * direction * radius);
            Gizmos.DrawRay(position, Quaternion.AngleAxis(-angle / 2f, Vector3.up) * direction * radius);
        }
#endif
    }


    // --- | Classes | -----------------------------------------------------------------------------------------------------

    /// <summary>
    /// A class to help the <see cref="FieldOfView"/> to filter its targets.
    /// </summary>
    public abstract class Filter
    {
        /// <summary>
        /// Compare the target with the filter.
        /// </summary>
        /// <param name="target">The target to check.</param>
        /// <returns>True if the target fits the criterias of the filter.</returns>
        public abstract bool Compare(Transform target);
    }

    /// <summary>
    /// Filters the targets of the <see cref="FieldOfView"/> by its tags.
    /// </summary>
    public class TagFilter : Filter
    {
        // Variables & Properties
        private string tag;
        /// <summary>
        /// The tag used to filter the targets.
        /// </summary>
        public string Tag
        {
            get => tag;
            set => tag = value;
        }

        // Constructor
        /// <summary>
        /// Creates a new filter.
        /// </summary>
        /// <param name="tag">The tag to filter the targets with.</param>
        public TagFilter(string tag)
        {
            this.tag = tag;
        }

        // Methods
        /// <summary>
        /// Compares if the tag of the target fits the filer.
        /// </summary>
        /// <param name="target">The target to check.</param>
        /// <returns>True if the tag is matching the filter.</returns>
        public override bool Compare(Transform target)
        {
            return target.tag == tag;
        }
    }
}
