﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour
{
    // --- | Variables & Properties | --------------------------------------------------------------------------------------

    private Camera mainCamera;


    // --- | Methods | -----------------------------------------------------------------------------------------------------

    private void Awake()
    {
        mainCamera = Camera.main;
    }

    private void Update()
    {
        transform.forward = mainCamera.transform.forward;
    }
}
