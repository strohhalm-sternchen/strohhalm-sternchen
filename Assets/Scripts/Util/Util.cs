﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Util
{
    public enum TimeFormat { SecondsOnly, MinutesAndSeconds }

    /// <summary>
    /// Converts a float to a time.
    /// </summary>
    /// <param name="time">The time in seconds.</param>
    /// <param name="format">The format to convert to.</param>
    /// <returns>The time converted.</returns>
    public static string FloatToTime(float time, TimeFormat format = TimeFormat.MinutesAndSeconds)
    {
        time = Mathf.CeilToInt(time);
        switch(format)
        {
            case TimeFormat.MinutesAndSeconds:
                int minuts = (int)(time / 60);
                int seconds = (int)(time % 60);
                return string.Format("{0}:{1}{2}", minuts, seconds / 10, seconds % 10);
            case TimeFormat.SecondsOnly:
                return time.ToString();
        }
        return "";
    }

    /// <summary>
    /// Calculates the difference in two angles.
    /// </summary>
    /// <param name="to">The angle to rotate to, in degrees.</param>
    /// <param name="from">The current angle, in degrees.</param>
    /// <returns>The difference in the two angles in degrees. Max 180 min -180.</returns>
    public static float AngleDistance(float to, float from)
    {
        float delta = to - from;
        return ClampAngle(delta);
    }

    /// <summary>
    /// Clams an angle between 180 and -180.
    /// </summary>
    /// <param name="angle">The angle to clamp.</param>
    /// <returns>The clmaped angle.</returns>
    public static float ClampAngle(float angle)
    {
        while (angle < -180f)
        {
            angle += 360f;
        }
        while (angle > 180f)
        {
            angle -= 360f;
        }
        return angle;
    }
}

public static class VectorExtention
{
    /// <summary>
    /// Sets the Y-Coordinate to 0.
    /// </summary>
    /// <param name="v">The vector to transform.</param>
    /// <returns>The grounded vector.</returns>
    public static Vector3 Grounded(this Vector3 v)
    {
        return new Vector3(v.x, 0f, v.z);
    }
}
public static class ArrayExtention
{
    public static T Random<T>(this T[] array)
    {
        return array[UnityEngine.Random.Range(0, array.Length)];
    }
}
