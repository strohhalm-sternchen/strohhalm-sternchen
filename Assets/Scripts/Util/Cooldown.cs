﻿using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class Cooldown
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [SerializeField, Tooltip("The max cooldown time.")]
    private float max;
    /// <summary>
    /// The max cooldown time.
    /// </summary>
    public float Max => max;

    /// <summary>
    /// Called when the cooldown starts.
    /// </summary>
    [Tooltip("Called when the cooldown starts.")]
    public UnityEvent OnCooldownStart = new UnityEvent();

    /// <summary>
    /// Called when the cooldown ends.
    /// </summary>
    [Tooltip("Called when the cooldown ends.")]
    public UnityEvent OnCooldownEnd = new UnityEvent();


    // --- | Variables & Properties | --------------------------------------------------------------------------------------

    private float current;
    /// <summary>
    /// The current time of the cooldown.
    /// </summary>
    public float Current => current;
    /// <summary>
    /// True if the cooldown is active.
    /// </summary>
    public bool IsOnCooldown => current > 0f;


    // --- | Constructor | -------------------------------------------------------------------------------------------------

    /// <summary>
    /// Creates a new <see cref="Cooldown"/> object.
    /// </summary>
    /// <param name="max">The time it takes the cooldown to reset.</param>
    public Cooldown(float max)
    {
        this.max = max;
    }

    // --- | Methods | -----------------------------------------------------------------------------------------------------

    /// <summary>
    /// Updates the cooldown.
    /// </summary>
    /// <param name="time">The delta time since the last update.</param>
    public void Update(float time)
    {
        if (IsOnCooldown)
        {
            current -= time;
            if (current < 0f)
            {
                current = 0f;
                OnCooldownEnd?.Invoke();
            }
        }
    }

    /// <summary>
    /// Sets the cooldown.
    /// </summary>
    public void Set()
    {
        current = max;
        OnCooldownStart?.Invoke();
    }
    /// <summary>
    /// Sets the cooldown and its max value.
    /// </summary>
    /// <param name="max"></param>
    public void Set(float max)
    {
        this.max = max;
        current = max;
        OnCooldownStart?.Invoke();
    }

    /// <summary>
    /// Resets the cooldown back to inactive.
    /// </summary>
    public void Reset()
    {
        current = 0f;
    }


    // --- | Operators | ---------------------------------------------------------------------------------------------------

    public static bool operator !(Cooldown cooldown)
    {
        return cooldown.IsOnCooldown;
    }
    public static explicit operator bool(Cooldown cooldown)
    {
        return cooldown.IsOnCooldown;
    }
    public static explicit operator float(Cooldown cooldown)
    {
        return cooldown.current;
    }
    public static bool operator >(Cooldown cooldown, float time)
    {
        return cooldown.current > time;
    }
    public static bool operator <(Cooldown cooldown, float time)
    {
        return cooldown.current < time;
    }
    public static bool operator >=(Cooldown cooldown, float time)
    {
        return cooldown.current >= time;
    }
    public static bool operator <=(Cooldown cooldown, float time)
    {
        return cooldown.current <= time;
    }
    public static bool operator >(float time, Cooldown cooldown)
    {
        return time > cooldown.current;
    }
    public static bool operator <(float time, Cooldown cooldown)
    {
        return time < cooldown.current;
    }
    public static bool operator >=(float time, Cooldown cooldown)
    {
        return time >= cooldown.current;
    }
    public static bool operator <=(float time, Cooldown cooldown)
    {
        return time <= cooldown.current;
    }
}
