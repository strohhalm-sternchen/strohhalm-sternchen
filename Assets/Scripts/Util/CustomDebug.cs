﻿namespace bugiflo.Debug
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEditor;

    public static class CustomDebug
    {
        /// <summary>
        /// Draws a rectangle into the 2D space.
        /// </summary>
        /// <param name="rect">The rect to draw.</param>
        public static void DrawRect(Rect rect)
        {
            DrawRect(rect, Color.white);
        }
        /// <summary>
        /// Draw a rectangle into the 2D space.
        /// </summary>
        /// <param name="rect">The rect to draw.</param>
        /// <param name="color">The rects color.</param>
        public static void DrawRect(Rect rect, Color color)
        {
            Debug.DrawLine(new Vector2(rect.xMin, rect.yMin), new Vector3(rect.xMin, rect.yMax), color);
            Debug.DrawLine(new Vector2(rect.xMax, rect.yMin), new Vector3(rect.xMax, rect.yMax), color);
            Debug.DrawLine(new Vector2(rect.xMin, rect.yMin), new Vector3(rect.xMax, rect.yMin), color);
            Debug.DrawLine(new Vector2(rect.xMin, rect.yMax), new Vector3(rect.xMax, rect.yMax), color);
        }

        /// <summary>
        /// Draws a point into the scene.
        /// </summary>
        /// <param name="point">The point to draw.</param>
        public static void DrawPoint(Vector3 point)
        {
            DrawPoint(point, Color.white);
        }
        /// <summary>
        /// Draws a point into the scene.
        /// </summary>
        /// <param name="point">The point ti draw.</param>
        /// <param name="maxSize">The points maximum size.</param>
        public static void DrawPoint(Vector3 point, float maxSize)
        {
            DrawPoint(point, maxSize, Color.white);
        }
        /// <summary>
        /// Draws a point into the scene.
        /// </summary>
        /// <param name="point">The point to draw.</param>
        /// <param name="color">The points color.</param>
        public static void DrawPoint(Vector3 point, Color color)
        {
#if (UNITY_EDITOR)
            DrawPoint(point, color, SceneView.lastActiveSceneView.camera.orthographicSize / 20f);
#endif
        }
        /// <summary>
        /// Draws a point into the scene.
        /// </summary>
        /// <param name="point">The point to draw.</param>
        /// <param name="maxSize">The points maximum size.</param>
        /// <param name="color">The points color.</param>
        public static void DrawPoint(Vector3 point, float maxSize, Color color)
        {
#if (UNITY_EDITOR)
            DrawPoint(point, color, Mathf.Clamp(SceneView.lastActiveSceneView.camera.orthographicSize / 20f, 0f, maxSize / 2));
#endif
        }
        /// <summary>
        /// The draw function for the DrawPoint functions.
        /// </summary>
        /// <param name="point">The point to draw.</param>
        /// <param name="color">The points color.</param>
        /// <param name="scale">The points scale.</param>
        private static void DrawPoint(Vector3 point, Color color, float scale)
        {
            Debug.DrawLine(point + Vector3.right * scale, point + Vector3.left * scale, color);
            Debug.DrawLine(point + Vector3.forward * scale / 2f, point + Vector3.up * scale / 2f, color);
            Debug.DrawLine(point + Vector3.up * scale / 2f, point + Vector3.back * scale / 2f, color);
            Debug.DrawLine(point + Vector3.back * scale / 2f, point + Vector3.down * scale / 2f, color);
            Debug.DrawLine(point + Vector3.down * scale / 2f, point + Vector3.forward * scale / 2f, color);

            Debug.DrawLine(point + Vector3.up * scale, point + Vector3.down * scale, color);
            Debug.DrawLine(point + Vector3.forward * scale / 2f, point + Vector3.right * scale / 2f, color);
            Debug.DrawLine(point + Vector3.right * scale / 2f, point + Vector3.back * scale / 2f, color);
            Debug.DrawLine(point + Vector3.back * scale / 2f, point + Vector3.left * scale / 2f, color);
            Debug.DrawLine(point + Vector3.left * scale / 2f, point + Vector3.forward * scale / 2f, color);

            Debug.DrawLine(point + Vector3.forward * scale, point + Vector3.back * scale, color);
            Debug.DrawLine(point + Vector3.up * scale / 2f, point + Vector3.right * scale / 2f, color);
            Debug.DrawLine(point + Vector3.right * scale / 2f, point + Vector3.down * scale / 2f, color);
            Debug.DrawLine(point + Vector3.down * scale / 2f, point + Vector3.left * scale / 2f, color);
            Debug.DrawLine(point + Vector3.left * scale / 2f, point + Vector3.up * scale / 2f, color);
        }

        /// <summary>
        /// Logs the keyframe into the console in a readable way.
        /// </summary>
        /// <param name="keyframe">The keyframe to log.</param>
        public static void LogKeyFrame(Keyframe keyframe)
        {
            /*Debug.LogFormat("Keyframe[{0}]:\nTime: {1}\nValue: {2}\nIn Tangent: {3}\nOut Tangent: {4}\nIn Weight: {5}\nOut Weight: {6}",
                i, keyframe.time, keyframe.value, keyframe.inTangent, keyframe.outTangent, keyframe.inWeight, keyframe.outWeight);*/
        }
    }
}
