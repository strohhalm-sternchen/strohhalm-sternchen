﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerArea : MonoBehaviour
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [SerializeField, Tooltip("True if the area should only trigger once.")]
    private bool triggerOnce = false;
    [Tooltip("Called when the player enters the area.")]
    public UnityEvent OnEnter;
    [Tooltip("Called when the player exits the area.")]
    public UnityEvent OnExit;


    // --- | Variables & Properties |---------------------------------------------------------------------------------------

    private bool triggerEnter = true;
    private bool triggerExit = true;


    // --- | Mehtods | -----------------------------------------------------------------------------------------------------

    private void OnTriggerEnter(Collider other)
    {
        if (triggerEnter && other.CompareTag("Player"))
        {
            OnEnter?.Invoke();
            triggerEnter = !triggerOnce;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (triggerExit && other.CompareTag("Player"))
        {
            OnExit?.Invoke();
            triggerExit = !triggerOnce;
        }
    }


    public void SetTriggerOnce(bool triggerOnce)
    {
        this.triggerOnce = triggerOnce;
    }
}
