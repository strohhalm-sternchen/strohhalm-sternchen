﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DrainUI : MonoBehaviour
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [SerializeField, Tooltip("The drain UIs conatiner.")]
    private RectTransform drainContaier;
    [SerializeField, Tooltip("The drain bar.")]
    private RectTransform drainBar;


    // --- | Methods | -----------------------------------------------------------------------------------------------------

    /// <summary>
    /// Shows the ui.
    /// </summary>
    /// <param name="position">The position where to display it.</param>
    public void Show(Vector3 position)
    {
        UpdateProgress(1f);
        drainContaier.position = position;
        drainContaier.gameObject.SetActive(true);

        Vector3 targetDirection = Camera.main.transform.position - transform.position;
        Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, 360, 0.0f);
        transform.rotation = Quaternion.LookRotation(newDirection);
    }

    /// <summary>
    /// Hides the UI.
    /// </summary>
    public void Hide()
    {
        drainContaier.localPosition = Vector3.zero;
        drainContaier.gameObject.SetActive(false);
    }

    /// <summary>
    /// Update the UI.
    /// </summary>
    /// <param name="progress">The current drain progress.</param>
    public void UpdateProgress(float progress)
    {
        drainBar.anchorMax = Vector2.right * progress + Vector2.up;
        drainBar.sizeDelta = Vector2.zero;
    }
}
