﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BossTrigger : MonoBehaviour
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [SerializeField, Tooltip("The boss to trigger.")]
    private EndBoss boss;

    [SerializeField, Tooltip("Triggered if the player enters the collider.")]
    public UnityEvent OnEnter;


    // --- | Methods | -----------------------------------------------------------------------------------------------------

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            boss.SetTarget(other.transform);
            gameObject.SetActive(false);
            OnEnter?.Invoke();
        }
    }
}
