﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Tip", menuName = "Tip")]
public class TutorialTip : ScriptableObject
{
    [SerializeField, TextArea, Tooltip("The white part of the message.")]
    private string whiteText;
    /// <summary>
    /// The white part of the message.
    /// </summary>
    public string WhiteText => whiteText;

    [SerializeField, TextArea, Tooltip("The red part of the message.")]
    private string redText;
    /// <summary>
    /// The red part of the message.
    /// </summary>
    public string RedText => redText;
}
