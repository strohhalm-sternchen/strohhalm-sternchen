﻿using System.Collections;
using UnityEngine;

public class BasicAttackParticleManager : MonoBehaviour
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [SerializeField, Tooltip("The time the particles are visible.")]
    public float duration;


    // --- | Variables & Properties | --------------------------------------------------------------------------------------

    private ParticleSystem[] particleSystems;
    private Animation anim;
    private Coroutine stopParticles;


    // --- | Methods | -----------------------------------------------------------------------------------------------------

    private void Awake()
    {
        particleSystems = GetComponentsInChildren<ParticleSystem>();
        anim = GetComponent<Animation>();

        foreach (ParticleSystem ps in particleSystems) {
            ps.Play();
        }
        anim.Play();

        if(stopParticles != null) {
            StopCoroutine(stopParticles);
        }

        stopParticles = StartCoroutine(Stop(duration));
    }

    /// <summary>
    /// Stops the Particle Animation.
    /// </summary>
    /// <param name="delay">The time until tha particles should be stopped.</param>
    /// <returns></returns>
    private IEnumerator Stop(float delay) {
        yield return new WaitForSeconds(delay);
        foreach (ParticleSystem ps in particleSystems)
        {
            ps.Stop();           
        }
        stopParticles = null;
        Destroy(transform.parent.gameObject, 0.5f);
    }
}
