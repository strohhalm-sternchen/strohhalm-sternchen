﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Drain : MonoBehaviour
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [Header("General")] // ------------------------------------------------------------------------
    [SerializeField, Tooltip("The time until the drain can be used again. Time in seconds.")]
    private Cooldown cooldown;
    [SerializeField, Tooltip("The size of the hitbox infront of the player.")]
    private Vector3 hitArea = new Vector3(1, 1, 2);
    [SerializeField, Tooltip("The time the spell lasts, in seconds.")]
    private Cooldown drainTime;
    [SerializeField, Tooltip("The amount of blood drained in one second.")]
    private float drainPerSecond = 20f;

    [Header("References")] // ---------------------------------------------------------------------
    [SerializeField, Tooltip("The interface displaying the drain progress.")]
    private DrainUI drainUI;
    [SerializeField, Tooltip("The vampires straw.")]
    private GameObject straw;
    [SerializeField, Tooltip("The script handling the animations.")]
    private VampireAnimationHandle animationHandle;

    [Header("Drain Events")] // -------------------------------------------------------------------
    [Tooltip("Called when the attack starts.")]
    public UnityEvent OnDrainStart;
    [Tooltip("Called while the attack is active.")]
    public DrainEvent OnDraining;
    [Tooltip("Called when the attack is done.")]
    public UnityEvent OnDrainStop;


    // --- | Variables & Properties | --------------------------------------------------------------------------------------

    private IDrainable drainable;
    /// <summary>
    /// True if the attack is active.
    /// </summary>
    public bool IsDraining => drainable != null;


    // --- | Methods | -----------------------------------------------------------------------------------------------------
#if (UNITY_EDITOR)
    // Editor -------------------------------------------------------------------------------------

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Vector3 v1 = transform.right * hitArea.x / 2f + transform.up * hitArea.y / 2f;
        Vector3 v2 = transform.forward * hitArea.z + transform.right * hitArea.x / 2f + transform.up * hitArea.y / 2f;

        Gizmos.DrawLine(transform.position + new Vector3(v1.x, v1.y, v1.z), transform.position + new Vector3(v2.x, v2.y, v2.z));
        Gizmos.DrawLine(transform.position + new Vector3(-v1.x, v1.y, v1.z), transform.position + new Vector3(-v2.x, v2.y, v2.z));
        Gizmos.DrawLine(transform.position + new Vector3(v1.x, -v1.y, v1.z), transform.position + new Vector3(v2.x, -v2.y, v2.z));
        Gizmos.DrawLine(transform.position + new Vector3(-v1.x, -v1.y, v1.z), transform.position + new Vector3(-v2.x, -v2.y, v2.z));
        
        Gizmos.DrawLine(transform.position + new Vector3(v2.x, v2.y, v2.z), transform.position + new Vector3(v2.x, -v2.y, v2.z));
        Gizmos.DrawLine(transform.position + new Vector3(v2.x, -v2.y, v2.z), transform.position + new Vector3(-v2.x, -v2.y, v2.z));
        Gizmos.DrawLine(transform.position + new Vector3(-v2.x, -v2.y, v2.z), transform.position + new Vector3(-v2.x, v2.y, v2.z));
        Gizmos.DrawLine(transform.position + new Vector3(-v2.x, v2.y, v2.z), transform.position + new Vector3(v2.x, v2.y, v2.z));

        Gizmos.DrawLine(transform.position + new Vector3(v1.x, v1.y, v1.z), transform.position + new Vector3(v1.x, -v1.y, v1.z));
        Gizmos.DrawLine(transform.position + new Vector3(v1.x, -v1.y, v1.z), transform.position + new Vector3(-v1.x, -v1.y, v1.z));
        Gizmos.DrawLine(transform.position + new Vector3(-v1.x, -v1.y, v1.z), transform.position + new Vector3(-v1.x, v1.y, v1.z));
        Gizmos.DrawLine(transform.position + new Vector3(-v1.x, v1.y, v1.z), transform.position + new Vector3(v1.x, v1.y, v1.z));
    }

#endif

    // MonoBehaviour ------------------------------------------------------------------------------

    private void Update()
    {
        if (cooldown.IsOnCooldown)
        {
            cooldown.Update(Time.deltaTime);
        }
        if (IsDraining)
        {
            drainTime.Update(Time.deltaTime);
            if (drainable.IsDainable)
            {
                float drainedBlood = drainable.Drain(drainPerSecond * Time.deltaTime);
                drainUI.UpdateProgress(1 - drainTime.Current / drainTime.Max);
                OnDraining?.Invoke(drainedBlood);
            }
            else
            {
                Cancle();
            }
            if (!drainTime.IsOnCooldown)
            {
                StopDrain();
            }
        }
    }

    // Interaction --------------------------------------------------------------------------------

    /// <summary>
    /// Checks if there is an object to drain from and starts the attack if so.
    /// </summary>
    public void CheckForEnemies()
    {
        if (!cooldown.IsOnCooldown)
        {
            foreach (RaycastHit collider in Physics.BoxCastAll(transform.position, hitArea, transform.forward, transform.rotation, 0f))
            {
                if (collider.transform != transform.parent)
                {
                    IDrainable drainable = collider.transform.GetComponent<IDrainable>();
                    if (drainable != null && drainable.IsDainable)
                    {
                        StartDrain(drainable);
                        return;
                    }
                }
            }
        }
    }

    /// <summary>
    /// Starts the drain attack.
    /// </summary>
    /// <param name="drainable">The object to drain from.</param>
    private void StartDrain(IDrainable drainable)
    {
        if (drainable.IsDainable)
        {
            this.drainable = drainable;
            straw.SetActive(true);
            OnDrainStart?.Invoke();
            drainTime.Set();
            cooldown.Set();
            animationHandle.SetDrain(true);
            drainUI.Show(drainable.transform.position);
        }
    }

    /// <summary>
    /// Stops the attack.
    /// </summary>
    private void StopDrain()
    {
        drainable = null;
        straw.SetActive(false);
        drainUI.Hide();
        animationHandle.SetDrain(false);
        drainTime.Reset();
        OnDrainStop?.Invoke();
    }

    /// <summary>
    /// Cancles the attack.
    /// </summary>
    public void Cancle() => StopDrain();


    // --- | Classes | -----------------------------------------------------------------------------------------------------

    /// <summary>
    /// A <see cref="UnityEvent"/> with a float as parameter.
    /// </summary>
    [System.Serializable]
    public class DrainEvent : UnityEvent<float> { }
}
