﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBasicAttack : MonoBehaviour
{
    // --- | Inpsector | ---------------------------------------------------------------------------------------------------

    [Header("Cultist")] // ------------------------------------------------------------------------
    [SerializeField, Tooltip("The attacks cooldown.")]
    private Cooldown cooldown;
    [SerializeField, Tooltip("The size of the hitbox infront of the entity.")]
    private Vector3 hitArea = new Vector3(1, 1, 2);
    [SerializeField, Tooltip("The time the attack stuns other enities.")]
    private float stunTime = 1f;
    [SerializeField, Tooltip("The amount of blood the entity has to play to cast the spell.")]
    private float healthCost = 5f;

    [Header("References")] // ---------------------------------------------------------------------
    [SerializeField, Tooltip("The vampire script of the entity.")]
    private Vampire vampire;
    [SerializeField, Tooltip("The gameobject with the particle effect.")]
    private GameObject attackEffect;
    [SerializeField, Tooltip("The animation handling script.")]
    private VampireAnimationHandle animationHandle;


    // --- | Variables & Properties | --------------------------------------------------------------------------------------

    /// <summary>
    /// The range of the attack.
    /// </summary>
    public float Range => hitArea.z;


    // --- | Methods | -----------------------------------------------------------------------------------------------------
#if (UNITY_EDITOR)
    // Editor -------------------------------------------------------------------------------------

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Vector3 v1 = transform.right * hitArea.x / 2f + transform.up * hitArea.y / 2f;
        Vector3 v2 = transform.forward * hitArea.z + transform.right * hitArea.x / 2f + transform.up * hitArea.y / 2f;

        Gizmos.DrawLine(transform.position + new Vector3(v1.x, v1.y, v1.z), transform.position + new Vector3(v2.x, v2.y, v2.z));
        Gizmos.DrawLine(transform.position + new Vector3(-v1.x, v1.y, v1.z), transform.position + new Vector3(-v2.x, v2.y, v2.z));
        Gizmos.DrawLine(transform.position + new Vector3(v1.x, -v1.y, v1.z), transform.position + new Vector3(v2.x, -v2.y, v2.z));
        Gizmos.DrawLine(transform.position + new Vector3(-v1.x, -v1.y, v1.z), transform.position + new Vector3(-v2.x, -v2.y, v2.z));

        Gizmos.DrawLine(transform.position + new Vector3(v2.x, v2.y, v2.z), transform.position + new Vector3(v2.x, -v2.y, v2.z));
        Gizmos.DrawLine(transform.position + new Vector3(v2.x, -v2.y, v2.z), transform.position + new Vector3(-v2.x, -v2.y, v2.z));
        Gizmos.DrawLine(transform.position + new Vector3(-v2.x, -v2.y, v2.z), transform.position + new Vector3(-v2.x, v2.y, v2.z));
        Gizmos.DrawLine(transform.position + new Vector3(-v2.x, v2.y, v2.z), transform.position + new Vector3(v2.x, v2.y, v2.z));

        Gizmos.DrawLine(transform.position + new Vector3(v1.x, v1.y, v1.z), transform.position + new Vector3(v1.x, -v1.y, v1.z));
        Gizmos.DrawLine(transform.position + new Vector3(v1.x, -v1.y, v1.z), transform.position + new Vector3(-v1.x, -v1.y, v1.z));
        Gizmos.DrawLine(transform.position + new Vector3(-v1.x, -v1.y, v1.z), transform.position + new Vector3(-v1.x, v1.y, v1.z));
        Gizmos.DrawLine(transform.position + new Vector3(-v1.x, v1.y, v1.z), transform.position + new Vector3(v1.x, v1.y, v1.z));
    }

#endif

    // MonoBehaviour ------------------------------------------------------------------------------

    private void Update()
    {
        cooldown.Update(Time.deltaTime);
    }

    // Attack -------------------------------------------------------------------------------------

    /// <summary>
    /// Casts the ability if off cooldown.
    /// </summary>
    public void Attack()
    {
        if(!cooldown.IsOnCooldown)
        {
            vampire.SubtractHealth(healthCost);
            animationHandle.TriggerAttack();
            Instantiate(attackEffect, transform.position, transform.rotation);
            CheckForEnemies();
            cooldown.Set();
        }
    }

    /// <summary>
    /// Find the enemies hit by the attack and interact with them.
    /// </summary>
    private void CheckForEnemies()
    {
        foreach (RaycastHit collider in Physics.BoxCastAll(transform.position + transform.forward * hitArea.z, hitArea, transform.forward, transform.rotation, 0f))
        {
            if (collider.transform != transform.parent)
            {
                IAttackable attackable = collider.transform.GetComponent<IAttackable>();
                if (attackable != null && attackable.IsHittable)
                {
                    attackable.Hit(1);
                }
                IStunable stunable = collider.transform.GetComponent<IStunable>();
                if (stunable != null)
                {
                    stunable.Stun(stunTime);
                }
            }
        }
    }
}
