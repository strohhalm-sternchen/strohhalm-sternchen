﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [SerializeField, Tooltip("The cameras target.")]
    private Transform target;


    // --- | Variables & Properties | --------------------------------------------------------------------------------------

    private Vector3 cameraPosition;


    // --- | Methods | -----------------------------------------------------------------------------------------------------

    private void LateUpdate()
    {
        if (target)
        {
            cameraPosition = target.position;
        }

        transform.position = cameraPosition;
    }
}
