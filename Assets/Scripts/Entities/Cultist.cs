﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cultist : Enemy
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [Header("Cultist")] // ------------------------------------------------------------------------
    [SerializeField, Tooltip("The force applied when hitting a target. " +
        "X representing the force away from the target, y representing the force upwards.")]
    private Vector2 knockback;

    [Header("Idle look")] // ----------------------------------------------------------------------
    [SerializeField, Tooltip("The time until the cultist rotates to a new direction.")]
    private FloatRange rotateTime = new FloatRange(2f, 5f);
    [SerializeField, Tooltip("The speed of the look-around-rotation.")]
    private FloatRange rotateSpeed = new FloatRange(5f, 360f);

    [Header("Target recognition")] // -------------------------------------------------------------
    [SerializeField, Tooltip("The area where the cultist can see the player.")]
    private FieldOfView viewArea;
    [SerializeField, Tooltip("The area where the cultist can hear the player.")]
    private FieldOfView hearingArea;


    // --- | Variables & Properties | --------------------------------------------------------------------------------------

    // Idle roation -------------------------------------------------------------------------------
    /// <summary>
    /// The current time to the next rotation.
    /// </summary>
    private Cooldown lookAround = new Cooldown(2.5f);
    private float targetRotation;

    // Target selection ---------------------------------------------------------------------------
    private FieldOfView.TagFilter targetFilter = new FieldOfView.TagFilter("Player");


    // --- | Methods | -----------------------------------------------------------------------------------------------------

#if (UNITY_EDITOR)
    // Editor -------------------------------------------------------------------------------------
    private void OnDrawGizmosSelected()
    {
        viewArea.DrawGizmos(transform, Color.red);
        hearingArea.DrawGizmos(transform, Color.blue);
    }
#endif

    // MonoBehaviour ------------------------------------------------------------------------------

    protected override void Awake()
    {
        base.Awake();
        lookAround.OnCooldownEnd.AddListener(LookAround);
        targetRotation = transform.eulerAngles.y;
    }

    protected override void Update()
    {
        // Check the area for a target.
        if (!HasTarget)
        {
            SetTarget(CheckForTartet());
        }
        // Call the base update.
        base.Update();
        // Idle
        if (isAlive && !target)
        {
            // Update the cooldown for a new rotation.
            if (!lookAround.IsOnCooldown)
            {
                lookAround.Set();
            }
            else
            {
                lookAround.Update(Time.deltaTime);
            }

            // Update the cultists rotation.
            float delta = Util.AngleDistance(targetRotation, transform.eulerAngles.y);
            float step;
            if (delta < 0)
            {
                step = Mathf.Clamp(delta * Time.deltaTime, -rotateSpeed.max * Time.deltaTime, -rotateSpeed.min * Time.deltaTime);
            }
            else
            {
                step = Mathf.Clamp(delta * Time.deltaTime, rotateSpeed.min * Time.deltaTime, rotateSpeed.max * Time.deltaTime);
            }
            if (Mathf.Abs(step) > Mathf.Abs(delta))
            {
                step = delta;
            }
            transform.Rotate(Vector3.up * step);
        }

    }

    // Target selection ---------------------------------------------------------------------------

    /// <summary>
    /// Get targets in vision or hearing range.
    /// </summary>
    /// <returns>The targets in range.</returns>
    private Transform CheckForTartet()
    {
        Transform[] targets = hearingArea.GetTargets(transform, targetFilter);
        if (targets.Length > 0)
        {
            return targets[0];
        }
        targets = viewArea.GetTargets(transform, targetFilter);
        if (targets.Length > 0)
        {
            return targets[0];
        }
        return null;
    }

    /// <summary>
    /// Hit the cultist. Kills the cultis.
    /// </summary>
    /// <param name="damage">The damage delt to the cultist.</param>
    public override void Hit(int damage) => Die();

    /// <summary>
    /// Performs an attack.
    /// </summary>
    /// <param name="target">The target to hit.</param>
    public override void Attack(Transform target)
    {
        target.GetComponent<Player>().Hit(damage);
        controller.ApplyForce((transform.position - target.position).Grounded().normalized * knockback.x + Vector3.up * knockback.y);
    }

    /// <summary>
    /// Get a new rotation for to rotate to, when idling.
    /// </summary>
    private void LookAround()
    {
        targetRotation = Random.Range(-180f, 180f);
        lookAround.Set(Random.Range(rotateTime.min, rotateTime.max));
    }

    /// <summary>
    /// Sets the cultists target.
    /// </summary>
    /// <param name="target">The cultists target.</param>
    public override void SetTarget(Transform target)
    {
        if (!HasTarget)
        {
            base.SetTarget(target);
        }
    }
}
