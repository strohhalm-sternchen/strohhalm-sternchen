﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class Enemy : MonoBehaviour, IAttackable, IDrainable
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [Header("Enemy")] // --------------------------------------------------------------------------
    [SerializeField, Tooltip("The amount of blood in the enemy.")]
    protected float blood = 20;
    [SerializeField, Tooltip("The enemies damage.")]
    protected int damage;
    [SerializeField, Tooltip("The enemies attack range.")]
    protected float attackRange = 1f;
    [SerializeField, Tooltip("The cooldown of the enemies attack.")]
    protected Cooldown attackCD = new Cooldown(0.5f);

    [Header("References")] // ---------------------------------------------------------------------
    [SerializeField, Tooltip("The script handling the enemies animaions.")]
    protected AnimationHandle animationHandle;

    [Header("Events")] // -------------------------------------------------------------------------
    [SerializeField, Tooltip("Called when the enemy dies.")]
    public UnityEvent OnDeath;


    // --- | Components | --------------------------------------------------------------------------------------------------

    protected NavMeshInput input;
    protected CharacterMovement controller;


    // --- | Variables & Propeties | ---------------------------------------------------------------------------------------

    // Resources ----------------------------------------------------------------------------------
    protected bool isAlive = true;
    public float Blood => blood;
    private float deathTime = 0f;

    // Interaction --------------------------------------------------------------------------------
    /// <summary>
    /// True if the enemy is hitable.
    /// </summary>
    public bool IsHittable => isAlive;
    /// <summary>
    /// True if the enemy is drainable.
    /// </summary>
    public bool IsDainable => !isAlive && blood > 0f;

    // Target -------------------------------------------------------------------------------------
    protected Transform target;
    /// <summary>
    /// The enemies target.
    /// </summary>
    public Transform Target => target;
    /// <summary>
    /// True if the enemy has a target.
    /// </summary>
    public bool HasTarget => target;


    // --- | Methods | -----------------------------------------------------------------------------------------------------
    // MonoBehavoir -------------------------------------------------------------------------------

    protected virtual void Awake()
    {
        input = GetComponent<NavMeshInput>();
        controller = GetComponent<CharacterMovement>();
    }

    protected virtual void Update()
    {
        if (isAlive)
        {
            attackCD.Update(Time.deltaTime);
            if (target && (target.position - transform.position).magnitude <= attackRange)
            {
                input.Stop();
                if (!attackCD.IsOnCooldown)
                {
                    Attack(target);
                    attackCD.Set();
                }
            }
            else
            {
                input.SetTarget(target);
            }
        }
        else
        {
            deathTime += Time.deltaTime;
            if (deathTime > 10f)
            {
                transform.position += Vector3.down * Time.deltaTime;
                if (deathTime > 13)
                {
                    Destroy(gameObject);
                }
            }
        }
    }

    // Attack -------------------------------------------------------------------------------------

    /// <summary>
    /// Deal damage to the enemy.
    /// </summary>
    /// <param name="damage">The damage to deal.</param>
    public abstract void Hit(int damage);

    /// <summary>
    /// Lets the enemy attack.
    /// </summary>
    /// <param name="target">The target to hit.</param>
    public abstract void Attack(Transform target);

    /// <summary>
    /// Kills the enemy.
    /// </summary>
    protected virtual void Die()
    {
        controller.EnabledMovement = false; 
        animationHandle.TriggerDeath();
        BloodManager.SpawnStainBelow(transform.position);
        controller.enabled = false;
        //BloodManager.SpawnSplashBelow(transform.position);
        isAlive = false;
        OnDeath?.Invoke();
    }

    /// <summary>
    /// Drains the enemy.
    /// </summary>
    /// <param name="amount">The amount of blood to drain.</param>
    /// <returns>the amount of drained blood.</returns>
    public float Drain(float amount)
    {
        float drainedBlood = blood;
        blood -= amount;
        if (blood <= 0f)
        {
            blood = 0f;
        }
        drainedBlood -= blood;
        return drainedBlood;
    }

    /// <summary>
    /// Sets the enemies target.
    /// </summary>
    /// <param name="target">The new target of the enemy.</param>
    public virtual void SetTarget(Transform target)
    {
        this.target = target;
    }
}
