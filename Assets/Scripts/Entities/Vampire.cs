﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class Vampire : MonoBehaviour, IStunable, IDrainable
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [Header("Vampire")] // ------------------------------------------------------------------------
    [SerializeField, Tooltip("The vampires health / blood.")]
    protected float health = 120f;
    [SerializeField, Tooltip("The time until the vampire can attack again after draining.")]
    protected Cooldown drainDelay = new Cooldown(0.5f);

    [Header("Heath Events")] // -------------------------------------------------------------------
    public HealthChange OnHealthChange;
    public UnityEvent OnDeath;

    [Header("Stun Events")] // --------------------------------------------------------------------
    public UnityEvent OnStunStart;
    public UnityEvent OnStunEnd;

    [Header("Attacks")] // ------------------------------------------------------------------------
    [SerializeField, Tooltip("The vampires basic attack.")]
    protected PlayerBasicAttack basicAttack;
    [SerializeField, Tooltip("The vampires drain attack.")]
    protected Drain drainAttack;

    [Header("References")] // ---------------------------------------------------------------------
    [SerializeField, Tooltip("The movement controlling script.")]
    protected VampireMovement controller;
    [SerializeField, Tooltip("The animation handling script.")]
    protected VampireAnimationHandle animationHandle;


    // --- | Variables & Properties | --------------------------------------------------------------------------------------

    // Heath --------------------------------------------------------------------------------------
    protected bool isAlive = true;
    protected float maxHealth;
    /// <summary>
    /// The vampitres max healt.
    /// </summary>
    public float MaxHealth => maxHealth;
    /// <summary>
    /// The vampires health.
    /// </summary>
    public float Health
    {
        get => health;
        protected set
        {
            bool isChange = health != value;
            health = value;
            if (health <= 0f)
            {
                health = 0f;
                Die();
            }
            if (health > maxHealth)
            {
                health = maxHealth;
            }
            if (isChange)
            {
                OnHealthChange?.Invoke(health);
            }
        }
    }
    /// <summary>
    /// The vampires blood.
    /// </summary>
    public float Blood => health;

    // Stun ---------------------------------------------------------------------------------------
    protected float stunnTime = 0f;
    /// <summary>
    /// True if the vampire is stunned.
    /// </summary>
    public bool IsStunned => stunnTime > 0f;

    // Drain --------------------------------------------------------------------------------------
    /// <summary>
    /// True if the vampire is draining.
    /// </summary>
    public bool IsDainable => IsStunned && health > 0f;

    // Movement -----------------------------------------------------------------------------------
    /// <summary>
    /// True if the vampire can move.
    /// </summary>
    protected virtual bool MoveEnabled => isAlive && !IsStunned && !drainAttack.IsDraining;


    // --- | Components | -------------------------------------------------------------------------
    


    // --- | Methods | -----------------------------------------------------------------------------------------------------
    // MonoBehaviour ------------------------------------------------------------------------------

    protected virtual void Awake()
    {
        maxHealth = health;
        drainAttack.OnDrainStart.AddListener(CheckMovement);
        drainAttack.OnDraining.AddListener(AddToHealth);
        drainAttack.OnDrainStop.AddListener(CheckMovement);
        drainAttack.OnDrainStop.AddListener(drainDelay.Set);
    }

    protected virtual void Update()
    {
        if (stunnTime > 0f)
        {
            stunnTime -= Time.deltaTime;
            if (stunnTime <= 0f)
            {
                stunnTime = 0f;
                CheckMovement();
                OnStunEnd?.Invoke();
                animationHandle.StopStun();
            }
        }
        drainDelay.Update(Time.deltaTime);
    }

    // Health -------------------------------------------------------------------------------------

    /// <summary>
    /// Substracts from the vampires health.
    /// </summary>
    /// <param name="amount">The amoubt of health to substract.</param>
    public void SubtractHealth(float amount)
    {
        Health -= amount;
    }

    /// <summary>
    /// Adds to the vampires health.
    /// </summary>
    /// <param name="health">The amount of health to add.</param>
    private void AddToHealth(float health)
    {
        Health += health;
    }

    /// <summary>
    /// Kills the vampire.
    /// </summary>
    protected virtual void Die()
    {
        isAlive = false;
        controller.enabled = false;
        if (IsStunned)
        {
            animationHandle.StopStun();
        }
        CheckMovement();
        animationHandle.TriggerDeath();
        OnDeath?.Invoke();
    }

    // Interaction --------------------------------------------------------------------------------

    /// <summary>
    /// Stuns the vampire.
    /// </summary>
    /// <param name="duration">The duration of the stun.</param>
    public void Stun(float duration)
    {
        if (isAlive)
        {
            if (!IsStunned)
            {
                animationHandle.SetStunned();
            }
            stunnTime = duration;
            CheckMovement();
            OnStunStart?.Invoke();
        }
    }

    /// <summary>
    /// Drains blood from the vampire.
    /// </summary>
    /// <param name="amount">The amount of blood to drain.</param>
    /// <returns>The amount of drained blood.</returns>
    public float Drain(float amount)
    {
        float drainedHealth = health;
        Health -= amount;
        drainedHealth -= health;
        return drainedHealth;
    }

    /// <summary>
    /// Enables and disables the vampires movement.
    /// </summary>
    protected void CheckMovement()
    {
        controller.EnabledMovement = MoveEnabled;
    }

    /// <summary>
    /// Deals damage to the vampite.
    /// </summary>
    /// <param name="damage">The amount of damage to deal.</param>
    public void Hit(int damage)
    {
        if (isAlive)
        {
            if (health > damage)
            {
                animationHandle.TriggerHit();
            }
            Health -= damage;
            if (isAlive && drainAttack.IsDraining)
            {
                drainAttack.Cancle();
            }
            BloodManager.SpawnStainBelow(transform.position);
        }
    }

    /// <summary>
    /// Lets the vampire attack.
    /// </summary>
    public void Attack()
    {
        basicAttack.Attack();
    }

    /// <summary>
    /// Lets the vampire drain.
    /// </summary>
    public void DrainAttack()
    {
        drainAttack.CheckForEnemies();
    }

    // --- | Classes | -----------------------------------------------------------------------------------------------------

    [System.Serializable]
    public class HealthChange : UnityEvent<float> { }
}
