﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Vampire, IAttackable
{
    // --- | Inpsector | ---------------------------------------------------------------------------------------------------

    [Header("Player")] // -------------------------------------------------------------------------
    [SerializeField, Tooltip("The time until the player drops blood on the floor.")]
    private Cooldown bloodDropTime = new Cooldown(0.2f);


    // --- | Variables & Properties | --------------------------------------------------------------------------------------

    /// <summary>
    /// True if the player can be hit.
    /// </summary>
    public bool IsHittable => isAlive;


    // --- | Methods | -----------------------------------------------------------------------------------------------------
    // MonoBehaviour ------------------------------------------------------------------------------

    protected override void Update()
    {
        if (isAlive)
        {
            base.Update();
            Health -= Time.deltaTime;
            if (bloodDropTime.IsOnCooldown)
            {
                bloodDropTime.Update(Time.deltaTime);
            }
            else
            {
                BloodManager.SpawnStainBelow(transform.position);
                bloodDropTime.Set();
            }
        }
    }
}
