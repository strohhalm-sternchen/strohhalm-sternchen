﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Demon : Enemy
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [Header("Demon")]
    [SerializeField, Tooltip("The hitpoints of the demon, the hits he can take.")]
    private int hitpoints = 3;


    // --- | Methods | -----------------------------------------------------------------------------------------------------

    /// <summary>
    /// Performs the demons attack.
    /// </summary>
    /// <param name="target">The target to hit.</param>
    public override void Attack(Transform target)
    {
        if (isAlive)
        {
            target.GetComponent<Player>().Hit(damage);
            animationHandle.TriggerAttack();
        }
    }

    /// <summary>
    /// Deal damage to the demon.
    /// </summary>
    /// <param name="damage">The damage to deal.</param>
    public override void Hit(int damage)
    {
        hitpoints -= damage;
        if (hitpoints <= 0)
        {
            Die();
        }
        else
        {
            animationHandle.TriggerHit();
        }
    }
}
