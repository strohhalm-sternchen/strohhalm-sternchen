﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndBoss : Vampire
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [Header("End Boss")] // -----------------------------------------------------------------------
    [SerializeField, Range(0f, 1f), Tooltip("The health range where the boss changes his behaviour to a more defenxive way.")]
    private float criticalHealthRange = 0.25f;

    [Header("Target Areas")] // -------------------------------------------------------------------
    [SerializeField, Tooltip("The area where the boss can see.")]
    private FieldOfView viewArea;
    [SerializeField, Tooltip("The area where the boss can hear.")]
    private FieldOfView soundArea;


    // --- | Components | --------------------------------------------------------------------------------------------------
    
    protected NavMeshInput input;


    // --- | Variables & Components | --------------------------------------------------------------------------------------

    protected Transform Target => input.Target;
    private ComponentFilter viewFilter = new ComponentFilter();


    // --- | Methods | -----------------------------------------------------------------------------------------------------
#if (UNITY_EDITOR)
    // Editor -------------------------------------------------------------------------------------
    private void OnDrawGizmosSelected()
    {
        viewArea.DrawGizmos(transform, Color.red);
        soundArea.DrawGizmos(transform, Color.blue);
    }
#endif

    // MonoBehaviour ------------------------------------------------------------------------------

    protected override void Awake()
    {
        base.Awake();
        input = GetComponent<NavMeshInput>();
    }

    protected override void Update()
    {
        base.Update();
        if (isAlive)
        {
            // Check the health for behaviour change.
            if (Target && health / maxHealth <= criticalHealthRange)
            {
                Transform[] transforms = soundArea.GetTargets(transform, viewFilter);
                foreach (Transform target in transforms)
                {
                    SetTarget(target);
                    if (target.tag != "Player")
                    {
                        break;
                    }
                }
                if (Target.tag == "Player")
                {
                    transforms = viewArea.GetTargets(transform, viewFilter);
                    foreach (Transform target in transforms)
                    {
                        SetTarget(target);
                        if (target.tag != "Player")
                        {
                            break;
                        }
                    }
                }
            }
            if (drainDelay.IsOnCooldown)
            {
                // TODO: Move away.
            }
            else if (Target && (Target.position.Grounded() - basicAttack.transform.position.Grounded()).magnitude <= basicAttack.Range - 0.5f)
            {
                Attack();
                DrainAttack();
            }
            else
            {
                input.SetTarget(Target);
            }
        }
    }

    /// <summary>
    /// Sets the bosses target.
    /// </summary>
    /// <param name="target">The target the boss should attack.</param>
    public void SetTarget(Transform target) => input.SetTarget(target);

    /// <summary>
    /// Kills the boss.
    /// </summary>
    protected override void Die()
    {
        base.Die();
        Vector3 reset = transform.eulerAngles;
        reset.x = 0f;
        reset.z = 0f;
        transform.eulerAngles = reset;
    }

    /// <summary>
    /// Disables the boss.
    /// </summary>
    public void Disable()
    {
        enabled = false;
    }

    // --- | Classes | -----------------------------------------------------------------------------------------------------

    public class ComponentFilter : FieldOfView.Filter
    {
        public override bool Compare(Transform target)
        {
            IDrainable drainable = target.GetComponent<IDrainable>();
            IAttackable attackable = target.GetComponent<IAttackable>();
            return drainable != null && drainable.IsDainable || attackable != null && attackable.IsHittable;
        }
    }
}
