﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAnimationHandler : VampireAnimationHandle
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [SerializeField, Tooltip("The particle system triggered by the death animation.")]
    public ParticleSystem deathFire;


    // --- | Methods | -----------------------------------------------------------------------------------------------------

    /// <summary>
    /// Triggers the death animation.
    /// </summary>
    public override void TriggerDeath()
    {
        if (animateDeath)
        {
            base.TriggerDeath();
            deathFire.gameObject.SetActive(true);
            deathFire.Play();
        }
    }
}
