﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationHandle : MonoBehaviour
{
    // --- | Inpsector | ---------------------------------------------------------------------------------------------------

    [SerializeField, Tooltip("The animator of the entity.")]
    protected Animator controller;

    [Header("Animated actions.")]
    [SerializeField, Tooltip("True if the entity has a death animation.")]
    protected bool animateDeath = false;
    [SerializeField, Tooltip("True if the entity has a attack animation.")]
    protected bool animateAttack = false;
    [SerializeField, Tooltip("True if the entity has a hit / impact animation.")]
    protected bool animateHit = false;
    [SerializeField, Tooltip("True if the entity has a move animation.")]
    protected bool animateMove = false;


    // --- | Variables & Properties | --------------------------------------------------------------------------------------

    /// <summary>
    /// The parameter name in the animator, that triggers the death animation.
    /// </summary>
    protected const string DEATH_LABLE = "Death";
    /// <summary>
    /// The parameter name in the animator, that triggers the attack animation.
    /// </summary>
    protected const string ATTACK_LABLE = "Attack";
    /// <summary>
    /// The parameter name in the animator, that triggers the hit / impact animation.
    /// </summary>
    protected const string HIT_LABLE = "Hit";
    /// <summary>
    /// The parameter name in the animator, that triggers the move animation.
    /// </summary>
    protected const string MOVE_LABLE = "Movement";


    // --- | Methods | -----------------------------------------------------------------------------------------------------

    /// <summary>
    /// Triggers the death animation.
    /// </summary>
    public virtual void TriggerDeath() { if (animateDeath) { controller.SetTrigger(DEATH_LABLE); } }
    /// <summary>
    /// Triggers the attack animation.
    /// </summary>
    public virtual void TriggerAttack() { if (animateAttack) { controller.SetTrigger(ATTACK_LABLE); } }
    /// <summary>
    /// Triggers the hit / impact animation.
    /// </summary>
    public virtual void TriggerHit() { if (animateHit) { controller.SetTrigger(HIT_LABLE); } }
    /// <summary>
    /// Updates the movement animation state. Toggle between idle and move.
    /// </summary>
    /// <param name="move">The current movement of the character.</param>
    public virtual void UpdateMove(Vector3 move) { if (animateMove) { controller.SetFloat(MOVE_LABLE, move.magnitude); } }
}
