﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VampireAnimationHandle : AnimationHandle
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [SerializeField, Tooltip("True if the entity has a stun animation.")]
    protected bool animateStun = false;

    [Header("References")]
    [SerializeField, Tooltip("The particle system triggered by the stun animaion.")]
    protected GameObject stunParticle;


    // --- | Variables & Properties | --------------------------------------------------------------------------------------

    /// <summary>
    /// The parameter name in the animator, that triggers the drain animation.
    /// </summary>
    protected const string DRAIN_LABLE = "Drain";
    /// <summary>
    /// The parameter name in the animator, that sets the stun state.
    /// </summary>
    protected const string STUNNED_LABLE = "Stunned";
    /// <summary>
    /// The parameter name in the animator, that triggers the stun animation.
    /// </summary>
    protected const string STUN_LABLE = "Stun";


    // --- | Methods | -----------------------------------------------------------------------------------------------------

    /// <summary>
    /// Triggers the drain animation.
    /// </summary>
    /// <param name="isDraining">True if the character is draining, false if the stopped.</param>
    public void SetDrain(bool isDraining) => controller.SetBool(DRAIN_LABLE, isDraining);

    /// <summary>
    /// Starts the stun animation.
    /// </summary>
    public void SetStunned()
    {
        if (animateStun)
        {
            controller.SetBool(STUNNED_LABLE, true);
            controller.SetTrigger(STUN_LABLE);
            stunParticle.SetActive(true);
        }
    }

    /// <summary>
    /// Stops the stun animation.
    /// </summary>
    public void StopStun()
    {
        if (animateStun)
        {
            controller.SetBool(STUNNED_LABLE, false);
            stunParticle.SetActive(false);
        }
    }
}
