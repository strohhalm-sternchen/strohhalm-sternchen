﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [SerializeField, Tooltip("True if the spawner is active.")]
    private bool isActive = true;
    [SerializeField, Tooltip("The time between spawning.")]
    private Cooldown cooldown;
    [SerializeField, Tooltip("The enemies spawned by the spawner. The length determins how many enemies can be spawned.")]
    private Enemy[] registerdEnemies;
    [SerializeField, Tooltip("The enemies the spawner can spawn.")]
    private GameObject[] enemyTypes;
    [SerializeField, Tooltip("The target of the enemy when spawned.")]
    private Transform target = null;
    [SerializeField, Tooltip("The spawnpoints where to spawn the enemies.")]
    private Transform[] spawnPoints;

    // --- | Variables & Properties | --------------------------------------------------------------------------------------

    private int spawnedEnemies = 0;


    // --- | Methods | -----------------------------------------------------------------------------------------------------
    // MonoBehavour -------------------------------------------------------------------------------

    private void Start()
    {
        foreach(Enemy enemy in registerdEnemies)
        {
            if (enemy != null)
            {
                spawnedEnemies++;
            }
        }
    }

    private void Update()
    {
        if (isActive && spawnedEnemies < registerdEnemies.Length)
        {
            if (cooldown.IsOnCooldown)
            {
                cooldown.Update(Time.deltaTime);
            }
            else
            {
                cooldown.Set();
                SpawnEnemy();
            }
        }
    }

    // Spawn --------------------------------------------------------------------------------------

    /// <summary>
    /// Spawns an enemy.
    /// </summary>
    private void SpawnEnemy()
    {
        Transform spawnPoint = spawnPoints.Random();
        registerdEnemies[spawnedEnemies] = Instantiate(enemyTypes.Random(), spawnPoint.position, spawnPoint.rotation).GetComponent<Enemy>();
        int current = spawnedEnemies;
        registerdEnemies[spawnedEnemies].OnDeath.AddListener(delegate { RemoveEnemyFromList(current); });
        registerdEnemies[spawnedEnemies].SetTarget(target);
        spawnedEnemies++;
    }

    /// <summary>
    /// Removes an enemy from the list.
    /// </summary>
    /// <param name="index">The index of the enemy.</param>
    private void RemoveEnemyFromList(int index)
    {
        registerdEnemies[index] = null;
        for (int i = index + 1; i < registerdEnemies.Length; i++)
        {
            registerdEnemies[i - 1] = registerdEnemies[i];
            registerdEnemies[i] = null;
        }
        spawnedEnemies--;
    }

    // Active -------------------------------------------------------------------------------------

    /// <summary>
    /// Activates the spawner.
    /// </summary>
    public void SetActive()
    {
        isActive = true;
    }

    /// <summary>
    /// Deactivates the spawner.
    /// </summary>
    public void SetInactiv()
    {
        isActive = false;
    }
}
