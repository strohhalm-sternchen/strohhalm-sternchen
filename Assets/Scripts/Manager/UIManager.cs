﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [Header("Player")]
    [SerializeField, Tooltip("The reference to the player.")]
    private Player player;
    [SerializeField, Tooltip("The textfield to display the health of the player.")]
    private Text playerHealthDisplay;
    [SerializeField, Tooltip("The healthbar to display the health of the player.")]
    private Image[] playerHealthBar;

    [Header("Boss")]
    [SerializeField, Tooltip("The container of the bosses health display.")]
    private GameObject bossHealthContainer;
    [SerializeField, Tooltip("The healthbar to display the health of the boss.")]
    private Image remainingBossHealth;

    [Header("Screens")]
    [SerializeField, Tooltip("The win screen.")]
    private GameObject winScreen;
    [SerializeField, Tooltip("The game over screen.")]
    private GameObject gameOverScreen;
    [SerializeField, Tooltip("The menu screen.")]
    private GameObject menuScreen;

    [Header("Buttons")]
    [SerializeField, Tooltip("The button to select when enabling the win screen.")]
    private Button winFirstButton;
    [SerializeField, Tooltip("The button to select when enabling the game over screen.")]
    private Button gameOverFirstButton;
    [SerializeField, Tooltip("The button to select when enabling the menu screen.")]
    private Button menuFirstButton;
    [SerializeField, Tooltip("The button to select the input device.")]
    private Button controlButton;

    [Header("Fade")]
    [SerializeField, Tooltip("The black blend object.")]
    private Image blackBlend;
    [SerializeField, Tooltip("The blend curve for fading to black.")]
    private AnimationCurve blendCurve;

    [Header("Tip")]
    [SerializeField, Tooltip("The container object with the tip.")]
    private GameObject tipContainer;
    [SerializeField, Tooltip("The white tip-textfield")]
    private Text whiteText;
    [SerializeField, Tooltip("The red tip-textfield")]
    private Text redText;


    // --- | Variables & Properties | --------------------------------------------------------------------------------------

    private EndBoss boss;
    private bool fadeToBlack = false;
    private float blendTime = 0f;
    private bool showWinScreen = false;
    private bool showGameOverScreen = false;
    private Text controlText;


    // --- | Methods | -----------------------------------------------------------------------------------------------------
    // MonoBehaviour ------------------------------------------------------------------------------

    private void Awake()
    {
        winScreen.SetActive(false);
        gameOverScreen.SetActive(false);
        menuScreen.SetActive(false);
        player.OnHealthChange.AddListener(UpdatePlayerHealth);
        controlText = controlButton.GetComponentInChildren<Text>();
    }

    private void Update()
    {
        if (fadeToBlack)
        {
            blendTime += Time.deltaTime;
            blackBlend.color = new Color(0, 0, 0, blendCurve.Evaluate(blendTime));
            if ((showWinScreen || showGameOverScreen) && blendCurve.keys[blendCurve.length-1].time <= blendTime)
            {
                if (showWinScreen)
                {
                    winScreen.SetActive(showWinScreen);
                    showWinScreen = false;
                    winFirstButton.Select();
                }
                if (showGameOverScreen)
                {
                    gameOverScreen.SetActive(showGameOverScreen);
                    showGameOverScreen = false;
                    gameOverFirstButton.Select();
                }
            }
        }
    }

    // Player Health ------------------------------------------------------------------------------

    /// <summary>
    /// Updates the players health display.
    /// </summary>
    /// <param name="health">The current health of the player.</param>
    private void UpdatePlayerHealth(float health)
    {
        float progress = health / player.MaxHealth;
        playerHealthDisplay.text = Util.FloatToTime(health);
        foreach (Image bar in playerHealthBar)
        {
            bar.fillAmount = progress;
        }
    }

    // Boss Health --------------------------------------------------------------------------------

    /// <summary>
    /// Shows the bosses healthbar.
    /// </summary>
    /// <param name="boss">The boss whos healthbar should be displayed.</param>
    public void ShowBossHealth(EndBoss boss)
    {
        this.boss = boss;
        boss.OnHealthChange.AddListener(UpdateBossHealth);
        bossHealthContainer.SetActive(true);
    }

    /// <summary>
    /// Hides the bosses healthbar.
    /// </summary>
    public void HideBossHealth()
    {
        boss.OnHealthChange.RemoveListener(UpdateBossHealth);
        bossHealthContainer.SetActive(false);
    }

    /// <summary>
    /// Updates the bosses healthdisplay.
    /// </summary>
    /// <param name="health">The current health of the boss.</param>
    private void UpdateBossHealth(float health)
    {
        if (boss)
        {
            float healthPercent = health / boss.MaxHealth;
            remainingBossHealth.fillAmount = healthPercent;
        }
    }

    // Win & Lose Screen --------------------------------------------------------------------------

    /// <summary>
    /// Shows the win screen.
    /// </summary>
    public void ShowWinScreen()
    {
        showWinScreen = true;
        FadeToBlack();
    }

    /// <summary>
    /// Shows the game over screen.
    /// </summary>
    public void ShowGameOverScreen()
    {
        showGameOverScreen = true;
        FadeToBlack();
    }

    /// <summary>
    /// Shows the menu screen.
    /// </summary>
    public void ShowMenu()
    {
        menuScreen.SetActive(true);
        menuFirstButton.Select();
    }

    /// <summary>
    /// Hides the menu screen.
    /// </summary>
    public void HideMenu()
    {
        menuScreen.SetActive(false);
    }

    // Tip ----------------------------------------------------------------------------------------

    /// <summary>
    /// Prints a tip to the sceen.
    /// </summary>
    /// <param name="whiteMessage">The part of the message to display in white.</param>
    /// <param name="redMessage">The part of the message to display in red.</param>
    public void ShowTip(string whiteMessage, string redMessage)
    {
        tipContainer.SetActive(true);
        whiteText.text = whiteMessage;
        redText.text = redMessage;
    }

    /// <summary>
    /// Prints a tip to the sceen.
    /// </summary>
    /// <param name="tip">The tips content.</param>
    public void ShowTip(TutorialTip tip)
    {
        ShowTip(tip.WhiteText, tip.RedText);
    }

    /// <summary>
    /// Hides the tipfield.
    /// </summary>
    public void HideTip()
    {
        tipContainer.SetActive(false);
        whiteText.text = "";
        redText.text = "";
    }

    // Control selection --------------------------------------------------------------------------

    /// <summary>
    /// Enables or disables the control selection.
    /// </summary>
    /// <param name="enabled">True if the selection should be enabled.</param>
    public void EnableControlSelection(bool enabled)
    {
        controlButton.interactable = enabled;
    }

    /// <summary>
    /// Sets the name of the controls.
    /// </summary>
    /// <param name="name">The name of the controls.</param>
    public void SetControlName(string name)
    {
        controlText.text = "Controls: " + name;
    }

    // Fade ---------------------------------------------------------------------------------------

    /// <summary>
    /// Fades the screen to black.
    /// </summary>
    public void FadeToBlack()
    {
        fadeToBlack = true;
    }
}
