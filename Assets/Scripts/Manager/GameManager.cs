﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;


/// <summary>
/// The input device.
/// </summary>
public enum InputType { Keyboard, Gamepad }

public class GameManager : MonoBehaviour
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [SerializeField, Tooltip("The menu scene.")]
    private string menu;

    [SerializeField, Tooltip("Called when the game is won.")]
    public UnityEvent OnWin;
    [SerializeField, Tooltip("Called when the game is lost.")]
    public UnityEvent OnLose;

    [Header("Menu input")]
    [SerializeField, Tooltip("The button to open the menu with the keyboard.")]
    private KeyCode keyboardMenu = KeyCode.Escape;
    [SerializeField, Tooltip("The button to open the menu with the gamepad.")]
    private KeyCode gamepadMenu = KeyCode.JoystickButton6;


    [Header("References")]
    [SerializeField, Tooltip("The games user interface.")]
    private UIManager ui;
    [SerializeField, Tooltip("The players input controlls")]
    private PlayerInput playerInput;


    // --- | Variables & Properties | --------------------------------------------------------------------------------------

    private static InputType inputType = InputType.Keyboard;
    public enum GameState { Active, Paused, Won, Lost }
    private GameState gameState = GameState.Active;
    /// <summary>
    /// True if a gampad is connected.
    /// </summary>
    public bool GamepadPresent => Input.GetJoystickNames().Length > 0;


    // --- | Methods | -----------------------------------------------------------------------------------------------------

    private void Start()
    {
        playerInput.SetInputDevice(inputType);
        ui.EnableControlSelection(GamepadPresent);
    }

    private void Update()
    {
        if ((inputType == InputType.Keyboard && Input.GetKeyDown(keyboardMenu)) || 
            (inputType == InputType.Gamepad && Input.GetKeyDown(gamepadMenu)))
        {
            if (gameState == GameState.Active)
            {
                PauseGame();
            }
            else if (gameState == GameState.Paused)
            {
                ContinueGame();
            }
        }
    }

    // Game States --------------------------------------------------------------------------------

    /// <summary>
    /// Pauses the game.
    /// </summary>
    public void PauseGame()
    {
        if (gameState == GameState.Active)
        {
            gameState = GameState.Paused;
            Time.timeScale = 0f;
            ui.EnableControlSelection(GamepadPresent);
            ui.ShowMenu();
        }
    }

    /// <summary>
    /// Continues / unpauses the game.
    /// </summary>
    public void ContinueGame()
    {
        if (gameState == GameState.Paused)
        {
            gameState = GameState.Active;
            Time.timeScale = 1f;
            ui.HideMenu();
        }
    }

    /// <summary>
    /// Triggers the win game chain.
    /// </summary>
    public void WinGame()
    {
        if (gameState == GameState.Active)
        {
            gameState = GameState.Won;
            OnWin?.Invoke();
            ui.HideTip();
            ui.ShowWinScreen();
        }
    }

    /// <summary>
    /// Triggers the lose game chain.
    /// </summary>
    public void LoseGame()
    {
        if (gameState == GameState.Active)
        {
            gameState = GameState.Lost;
            OnLose?.Invoke();
            ui.HideTip();
            ui.ShowGameOverScreen();
        }
    }

    // Scene loading ------------------------------------------------------------------------------

    /// <summary>
    /// Go to main menu scene.
    /// </summary>
    public void LoadMenuScene()
    {
        SceneManager.LoadScene(menu);
        Time.timeScale = 1f;
    }

    /// <summary>
    /// Restarts the level.
    /// </summary>
    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        Time.timeScale = 1f;
    }

    // Input type ---------------------------------------------------------------------------------

    public void SetInputDevice(InputType device)
    {
        switch (device)
        {
            default: SetKeyboardActive(); break;
            case InputType.Gamepad: SetGamepadActive(); break;
        }
    }

    public void SetKeyboardActive()
    {
        inputType = InputType.Keyboard;
        playerInput.SetInputDevice(inputType);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        ui.SetControlName(inputType.ToString());
    }

    public void SetGamepadActive()
    {
        inputType = InputType.Gamepad;
        playerInput.SetInputDevice(inputType);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        ui.SetControlName(inputType.ToString());
    }

    public void SwapInput()
    {
        SetInputDevice((InputType)((int)(inputType + 1) % System.Enum.GetNames(typeof(InputType)).Length));
    }
}
