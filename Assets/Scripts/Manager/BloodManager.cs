﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodManager : MonoBehaviour
{
    // --- | Singelton | ---------------------------------------------------------------------------------------------------

    private static BloodManager instance;
    /// <summary>
    /// The singelton instance.
    /// </summary>
    public static BloodManager Instance => instance;


    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [Header("Gerneal")] // ------------------------------------------------------------------------
    [SerializeField, Tooltip("The time until a blood despawns.")]
    private float despawnTime = 5f;
    [SerializeField, Tooltip("The amount of stains that can be in the scene at once.")]
    private int poolLength = 50;

    [Header("Prefabs")] // ------------------------------------------------------------------------
    [SerializeField, Tooltip("The blood stain prefabs to spawn.")]
    private GameObject[] bloodStains;
    [SerializeField, Tooltip("The blood splash prefabs to spawn.")]
    private GameObject[] bloodSplashes;


    // --- | Variables & Properties | --------------------------------------------------------------------------------------

    private GameObject[] bloodPool;
    private static int bloodCount = 0;
    private static int currentBlood = 0;
    /// <summary>
    /// The bloodpool, containing all spawned blood prefabs.
    /// </summary>
    private static GameObject[] Blood
    {
        get => instance.bloodPool;
        set => instance.bloodPool = value;
    }


    // --- | Methods | -----------------------------------------------------------------------------------------------------

    private void OnEnable()
    {
        if (SetSingelton())
        {
            bloodPool = new GameObject[poolLength];
            bloodCount = 0;
            currentBlood = 0;
        }
    }

    private void OnDestroy()
    {
        if (instance == this)
        {
            instance = null;
        }
    }

    // Stain --------------------------------------------------------------------------------------

    /// <summary>
    /// Spawns a blood stain.
    /// </summary>
    /// <param name="position">The position where to spawn the stain.</param>
    /// <param name="normal">The normal of the object where to spawn it on.</param>
    public static void SpawnStain(Vector3 position, Vector3 normal)
    {
        try
        {
            Quaternion rotation = Quaternion.LookRotation(new Vector3(normal.y, normal.x, normal.z), normal);
            if (bloodCount < Blood.Length)
            {
                Blood[currentBlood] = Instantiate(instance.bloodStains.Random(), position, rotation, instance.transform);
                bloodCount++;
            }
            else
            {
                Blood[currentBlood].transform.position = position;
                Blood[currentBlood].transform.rotation = rotation;
                Blood[currentBlood].SetActive(true);
            }
            Vector3 euler = Blood[currentBlood].transform.localEulerAngles;
            euler.y = Random.Range(-180f, 180f);
            Blood[currentBlood].transform.localEulerAngles = euler;
            foreach (Collider collider in Physics.OverlapSphere(position, 0.5f))
            {
                if (collider.tag == "Blood")
                {
                    collider.GetComponent<IBloodInteractable>()?.InteractWithBlood();
                }
            }
            instance.StartCoroutine(instance.Despawn(Blood[currentBlood]));
            currentBlood = (currentBlood + 1) % Blood.Length;
        }
        catch (UnassignedReferenceException) { }
    }

    /// <summary>
    /// Spawn a blood stain.
    /// </summary>
    /// <param name="hit">The <see cref="RaycastHit"/> where to spawn the blood stain.</param>
    public static void SpawnStain(RaycastHit hit)
    {
        SpawnStain(hit.point, hit.normal);
    }

    /// <summary>
    /// Spawn a bloodsain under a position.
    /// </summary>
    /// <param name="position">The position to start the raycast to check for the next floor.</param>
    public static void SpawnStainBelow(Vector3 position)
    {
        RaycastHit hit;
        if(Physics.Raycast(position, Vector3.down, out hit))
        {
            SpawnStain(hit);
        }
    }

    // Splash -------------------------------------------------------------------------------------

    /// <summary>
    /// Spawns a blood splash below a positon.
    /// </summary>
    /// <param name="position">The position to start the raycast to check for the next floor.</param>
    /// <param name="direction">The direction the splash should face.</param>
    public static void SpawnSplashBelow(Vector3 position, Vector3 direction)
    {
        RaycastHit hit;
        if (Physics.Raycast(position, Vector3.down, out hit))
        {
            SpawnSplash(hit, direction);
        }
    }

    /// <summary>
    /// Spawns a blood splash.
    /// </summary>
    /// <param name="hit">The <see cref="RaycastHit"/> where to spawn the splash.</param>
    /// <param name="direction">The direction the splash should face.</param>
    public static void SpawnSplash(RaycastHit hit, Vector3 direction)
    {
        Quaternion rotation = Quaternion.LookRotation(new Vector3(hit.normal.x, hit.normal.z, hit.normal.y), direction);
        Instantiate(instance.bloodSplashes.Random(), hit.point, rotation).AddComponent<DestroyAfterTime>();
        foreach (Collider collider in Physics.OverlapBox(hit.point, new Vector3(1f, 1f, 2.5f), rotation))
        {
            if (collider.tag == "Blood")
            {
                collider.GetComponent<IBloodInteractable>()?.InteractWithBlood();
            }
        }
    }

    // Misc ---------------------------------------------------------------------------------------

    /// <summary>
    /// Despawns a bloodsain.
    /// </summary>
    /// <param name="blood">The stain to despawn.</param>
    /// <returns></returns>
    private IEnumerator Despawn(GameObject blood)
    {
        yield return new WaitForSeconds(despawnTime);
        if (blood)
        {
            blood.SetActive(false);
        }
    }

    // Init ---------------------------------------------------------------------------------------

    /// <summary>
    /// Sets the singelton instance.
    /// </summary>
    /// <returns>True if this is the singelton instance.</returns>
    private bool SetSingelton()
    {
        if (instance)
        {
            Debug.LogWarningFormat("There are mutible {0} in this scene.", GetType());
            enabled = false;
        }
        else
        {
            instance = this;
        }
        return enabled;
    }


    // --- | Classes | -----------------------------------------------------------------------------------------------------

    private class DestroyAfterTime : MonoBehaviour
    {
        private float despawnTime = 5f;

        private void Update()
        {
            despawnTime -= Time.deltaTime;
            if (despawnTime <= 0f)
            {
                Destroy(gameObject);
            }
        }
    }
}
