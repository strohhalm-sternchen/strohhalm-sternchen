﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class WallAlignment : EditorWindow
{
    bool toggle;
    static float xSnap;
    static float zSnap;

    [MenuItem("Window/Wall Alignment")]
    static void Init()
    {
        WallAlignment window = (WallAlignment)EditorWindow.GetWindow(typeof(WallAlignment));
        zSnap = 0.5f * Mathf.Sqrt(3f);
        xSnap = 1.5f;
        window.Show();
    }

    void OnGUI()
    {
        if (GUILayout.Button("Snap"))
        {
            foreach (GameObject selection in Selection.gameObjects)
            {
                Transform selectedObject = selection.transform;
                Vector3 snapPosition = selectedObject.transform.position;
                snapPosition.x = Mathf.Round(snapPosition.x / xSnap) * xSnap;
                snapPosition.z = Mathf.Round(snapPosition.z / zSnap) * zSnap;
                selectedObject.position = snapPosition;
            }
        }
    }
}